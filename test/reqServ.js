const expect = require('chai').expect;
const request = require('supertest');

module.exports = {

    choseRequest: (app, nameWorks, route, requestType, status,  POST) => {

        switch(requestType){
            case "GET":
                it(nameWorks, (done) => {
                    request(app)
                    .get(route)
                    .set('Content-Type', 'application/json')
                    .expect(status, done)
                })
                break;
            case "POST":
                it(nameWorks, (done) => {
                    request(app)
                    .post(route)
                    .send(POST)
                    .set('Content-Type', 'application/json')
                    .expect(status, done)
                })  
                break;
           } 
       }
}