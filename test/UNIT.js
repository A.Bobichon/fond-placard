const express = require('express');
const connection = require('../schema/models');
const app = express(); 
const bodyParser = require('body-parser');
const routeur = require("../router/router");
const expect = require('chai').expect;
const reqServ = require('./reqServ');

class UNIT {
    constructor(){
        this.app = app;
        this.con = connection;
    }

    test(nameTest, nameWorks, route, requestType, status,  POST) {
        describe(nameTest, () => {
            before((done) => {
                this.con.connectDB()
                .then(( ) =>  done())
                .catch((err) => done(err));
            });
            after((done) => {
                this.con.disconnectedDB()
                .then(() => done())
                .catch((err) => done(err));
            });

            reqServ.choseRequest(this.app, nameWorks, route, requestType, status,  POST)
        })   
    }
}

const UNITstart = new UNIT();
UNITstart.app.use(bodyParser.urlencoded({ extended: false }));
UNITstart.app.use(bodyParser.json());
routeur.myRouter(UNITstart.app);

module.exports = { UNITstart };