
const myUNIT = require('../../UNIT');
const UNIT = myUNIT.UNITstart;

// POST TEST
UNIT.test("POST RECIPE ....", "POST WORKS WELL !!!", "/recipe/post", "POST", 200, 
        {
            "name": "NOTOMATO",
            "category": "salée",
            "picture": "nop",
            "score": 145,
            "ingredient": [
                { "_id": "5f5f84b55d110dd298a73f7b" }
            ]
        }
);
UNIT.test("POST RECIPE ....", "NULL DOESN'T WORKS WELL !!!", "/recipe/post", "POST", 400, null);

// GET TEST
UNIT.test("GET ALL RECIPE ....", "GET ALL WORKS WELL!!!", "/recipe/all", "GET", 200);
UNIT.test("GET ONE BY id ING ....", "GET BY NAME WORKS WELL !!!", "/recipe/getOne/5f60c2e1dd564a7e555dbb14", "GET", 200);
UNIT.test("GET ONE BY ING NAME ....", "GET BY ING NAME WORKS WELL !!!", "/recipe/gerRepIng/tomate", "GET", 200);

