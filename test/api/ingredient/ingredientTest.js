
const myUNIT = require('../../UNIT');
const UNIT = myUNIT.UNITstart;

//POST TEST
UNIT.test("POST ING ....", "POST WORKS WELL !!!", "/ingredient/post", "POST", 200, { "name": "TEST" });
UNIT.test("POST ING ....", "NULL DOESN'T WORK", "/ingredient/post", "POST", 400, null);

//GET TEST
UNIT.test("GET ALL ING ....", "GET ALL WORKS WELL!!!", "/ingredient/all", "GET", 200);
UNIT.test("GET BY NAME ING ....", "GET BY NAME WORKS WELL !!!", "/ingredient/oneByName/tomate", "GET", 200);

