const repo = require('../repositoty/recipe');
const serialize = require("../service/serializer");

const ingredientController = { 
    post: async (req, res) => {
        const post = await repo.post(
            req.body.name,
            req.body.category,
            req.body.picture,
            req.body.score,
            req.body.ingredient
            )
        return res.json(post) 
    },

    getAll: async(req, res) => {        
        const all = await repo.findAll()
        return res.json(all);
    },

    getOne: async(req, res) => {
        const one = await repo.findOne(req.params.id);
        return res.json(one);
    },

    getByNameIng: async(req, res) => {
        const one = await repo.findByNameIng(req.params.name);
        return res.json(one);
    },

    deleteById: async (req, res) => {
        const deleted = await repo.deleteById(req.params.id);
        return res.json(deleted);
    },

    suppElemRec: async(req, res) => {
        const updated = await repo.suppElemRec(req.params.idR, req.params.idI);
        return res.json(updated);
    },

    addElem: async(req, res) => {
        const updated = await repo.addElem(req.params.idR, req.params.idI);
        return res.json(updated);
    }
}

module.exports = ingredientController;