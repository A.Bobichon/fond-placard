const repo = require('../repositoty/share');

const shareController = { 
    post: async (req, res) => {
        const post = await repo.post(
            req.body.note,
            req.body.text,
            req.params.id,
            )
        return res.json(post) 
    },

    findAll: async (req, res) => {
        const all = await repo.findAll();
        return res.json(all);
    }

}

module.exports = shareController;