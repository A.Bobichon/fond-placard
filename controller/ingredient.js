const repo = require("../repositoty/ingredients");

module.exports = { 
    post: (req, res) => { 
        const post = repo.post(req.body.name);
        return  res.json(post);
    },

    getAll: async(req, res) => {  
        const all = await repo.findAll();
        return res.json(all);
    },

    getOne: async (req, res) => {
        const ingrByName = await repo.getOneByName(req.params.name);
        return res.json(ingrByName);
    }
}