const express = require('express');
const connection = require('./schema/models');
const bodyParser = require('body-parser');
const routeur = require("./router/router");
const app = express(); 
const port = 8080;

connection.connectDB();

// parse application/x-www-form-urlencoded
app.use(bodyParser.urlencoded({ extended: false }));
app.use(bodyParser.json());
routeur.myRouter(app);


app.get('/', function(req, res) { 
    res.send('Hello world  ! ') 
});


app.listen(port, () =>  { 
    console.log('le serveur fonctionne :: ' + port)
})




