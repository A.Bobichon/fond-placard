const ingredient = require('../schema/model/ingredient');
const ingredientEntity = require('../schema/entity/ingredients');

module.exports = {
    findAll: async ()=>{
        let tab = [];
        const ingredient_all = await ingredient.find();

        ingredient_all.map( row => {
            const ing = new ingredientEntity(row._id, row.name);
            tab.push(ing)
        });

        return tab;

    },

    post: async (name)=>{
        const new_ingredient = await new ingredient({ name:name });
        new_ingredient.save()
        return new ingredientEntity(new_ingredient._id, new_ingredient.name);
    },

    getOneByName: async (name)=>{
        const ingredients = await ingredient.find({name:name});
        return new ingredientEntity(ingredients[0]._id, ingredients[0].name);
    }


}