const recipe = require('../schema/model/recipe');
const recipeEntity = require('../schema/entity/recipe');
const ingredient = require('../schema/model/ingredient');

module.exports = {
    findAll: async ()=>{
        let tab = [];
        const recipe_all = await recipe.find()
            .populate("ingredient");

            recipe_all.map( row => {
            const ing = new recipeEntity(
                row._id,
                row.name,
                row.category,
                row.picture,
                row.score,
                row.ingredient
                );
            tab.push(ing)
        });

        return tab;
    },

    findOne: async (id)=>{
        const myRecipe = await recipe.find({_id:id})
            .populate("ingredient")
            .populate("share")
        
        const ing = new recipeEntity(
            myRecipe[0]._id,
            myRecipe[0].name,
            myRecipe[0].category,
            myRecipe[0].picture,
            myRecipe[0].score,
            myRecipe[0].ingredient,
            myRecipe[0].share
            );

        return ing;
    },

    findByNameIng: async (name)=>{
        let tab = [];
        const ingredient_all = await ingredient.find({ name: name });

        for( const row of ingredient_all ) {
            let recipeData = await recipe.find({ingredient: row._id});
            recipeData.map( elem => {
                let ing = new recipeEntity(
                        elem._id,
                        elem.name,
                        elem.category,
                        elem.picture,
                        elem.score,
                        elem.ingredient
                    );
                
                tab.push(ing);
            })
        };
        return tab;     
    },

    post: async (name, category, picture, score, ingredient)=>{
       
        if (!name) {
            res.send('Il manque un argument')
            return null;
        }
    
        const new_recipe = await new recipe({
                    name:name,
                    category: category,
                    picture: picture,
                    score: score,
                    ingredient: ingredient
                });

        new_recipe.save() 

        return new recipeEntity(
            new_recipe._id,
            new_recipe.name,
            new_recipe.category,
            new_recipe.picture, 
            new_recipe.score, 
            new_recipe.ingredient);
        
    },

    suppElemRec: async (idRecipe, idElem) => {
        const myRecipe = await recipe.findById(idRecipe);
        myRecipe.ingredient.forEach( (element, index) => {
            if(String(element) === String(idElem)) myRecipe.ingredient.splice( index, 1 );
        });

        const entity = new recipeEntity(
            myRecipe._id,
            myRecipe.name,
            myRecipe.category,
            myRecipe.picture, 
            myRecipe.score, 
            myRecipe.ingredient
        );

        myRecipe.updateOne( 
            idRecipe, {
                ingredient: entity.ingredient
            }
        );

        myRecipe.save();
        return entity;
    },

    addElem: async (idRecipe, idElem) => {
        const myRecipe = await recipe.findById(idRecipe);
        myRecipe.ingredient.forEach( (element, index) => {
            if(String(element) !== String(idElem)) myRecipe.ingredient.push( idElem );
        });

        const entity = new recipeEntity(
            myRecipe._id,
            myRecipe.name,
            myRecipe.category,
            myRecipe.picture, 
            myRecipe.score, 
            myRecipe.ingredient
        );

        myRecipe.updateOne( 
            idRecipe, {
                ingredient: entity.ingredient
            }
        );

        myRecipe.save();
        return entity;
    },

    deleteById: async ( id ) => {
        await recipe.deleteOne({ id:id }, (err) => {
            if( err ) return handleError(err);
            return { };
        });

        

    }
}