const share = require("../schema/model/share");
const recipe = require("../schema/model/recipe");
const shareEntity = require('../schema/entity/share');


module.exports = {
    post: async (note, text, idR) => {

        const myShare = new shareEntity();
        myShare.note = note;
        myShare.text = text;
        myShare.conseiller = note <= 5 ? "Déconseiller": "Conseiller";
        const shareCreate = await new share({
            note: myShare.note,
            text: myShare.text,
            conseiller: myShare.conseiller,
        });
        shareCreate.save();
        myShare.id = shareCreate._id;
        const myRecipe = recipe.findByIdAndUpdate( idR,
            {
                $push: {
                    share: shareCreate._id
                }
            } , { new: true, useFindAndModify: false }
        );
        (await myRecipe).save()
        return shareCreate;
    },

    findAll: async ()=>{
        let tab = [];
        const share_all = await share.find()

            share_all.map( row => {
                const shareData = new shareEntity(
                    row.note,
                    row.text,
                    row.conseiller,
                );
            tab.push(shareData);
        });
        return tab;
    },

}