const ingredient = require("../router/routes/ingredients");
const recipe = require('../router/routes/recipes');
const share = require('../router/routes/share');

const routes = [
    {
        use: ingredient,
        path: "/ingredient"
    },
    {
        use: recipe,
        path: "/recipe"
    },
    {
        use: share,
        path: "/share"
    }
]

module.exports = { 
    myRouter: (app) => {
        /* controller path */
        routes.forEach( route => {
            app.use(route.path, route.use);
        });

    },

}
