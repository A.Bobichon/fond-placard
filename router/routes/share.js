const express = require('express');
const router = express.Router();
const controller = require("../../controller/share")

//POST
router.post('/post/:id', (req, res) => {
    controller.post(req, res);
});

//GET
router.get('/all', (req, res) => {
    controller.findAll(req, res);
});



module.exports = router;