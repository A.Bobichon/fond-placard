const express = require('express');
const router = express.Router();
const controller = require("../../controller/recipe")

//GET ALL
router.get('/all', (req, res) => {
    controller.getAll(req, res);
});

//POST
router.post('/post', (req, res) => {
    controller.post(req, res);
});

//GET ONE BY ID
router.get('/getOne/:id', (req, res) => {
    controller.getOne(req, res);
});

//GET ONE BY ING NAME
router.get('/gerRepIng/:name', (req, res) => {
    controller.getByNameIng(req, res);
});

//UPDATE SUP ELEM
router.delete('/supIng/:idR/:idI', (req, res) => {
    controller.suppElemRec(req, res);
});

//UPDATE ADD ELEM
router.put('/addIng/:idR/:idI', (req, res) => {
    controller.addElem(req, res);
});

//DELETE
router.delete('/deleteOneIng/:id', (req, res) => {
    controller.deleteById(req, res);
});

module.exports = router;