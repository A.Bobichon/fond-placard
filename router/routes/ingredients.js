const express = require('express');
const router = express.Router();
const controller = require("../../controller/ingredient");

//GET ALL
router.get('/all', (req, res) => {
    controller.getAll(req,res);
});

//POST
router.post('/post', (req, res) => {
    controller.post(req, res);
});

//GET ONE BY NAME
router.get("/oneByName/:name", (req, res) => {
    controller.getOne(req, res)
});



module.exports = router;
