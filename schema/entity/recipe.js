
class Recipe {   
    
    constructor(id,name, category, picture, score, ingredient, share){
        this.id = id;
        this.name = name;
        this.category = category;
        this.picture = picture;
        this.score = score;
        this.ingredient = ingredient;
        this.share = share;
    }

}

module.exports = Recipe;