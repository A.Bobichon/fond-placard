const mongoose = require('mongoose');

const ShareSchema  = mongoose.Schema({
    note: Number,
    text: String,
    conseiller: String,
})

const share = mongoose.model("share" ,ShareSchema)

module.exports = share;