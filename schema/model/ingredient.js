const mongoose = require('mongoose');

const ingredientSchema = new mongoose.Schema({
    name: String
})

const ingredient = mongoose.model("ingredient" ,ingredientSchema);

module.exports = ingredient;