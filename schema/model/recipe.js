const mongoose = require('mongoose');

const RecipeSchema  = mongoose.Schema({
    name: String,
    category: String,
    picture: String,
    score: Number,
    ingredient: [{type: mongoose.Schema.Types.ObjectId, ref: "ingredient"}],
    share: [{type: mongoose.Schema.Types.ObjectId, ref: "share"}]
})

const Recipe = mongoose.model("recipe" ,RecipeSchema)

module.exports = Recipe;