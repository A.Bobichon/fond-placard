const ingredient = require("./model/ingredient");
const recipe = require("./model/recipe");
const share = require("./model/share");
const mongoose = require("mongoose");

const connectDB = () => {
    try{
        return mongoose.connect("mongodb+srv://root:root@cluster0.fp6z2.mongodb.net/fond_placard?retryWrites=true&w=majority",  { useNewUrlParser: true, useUnifiedTopology: true  }  );
    } catch (err){
        console.log(err);
    }
};

const disconnectedDB = () => {
    return mongoose.disconnect();
}

const models = { ingredient, recipe, share }

module.exports = { models, connectDB, disconnectedDB }