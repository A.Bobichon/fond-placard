module.exports = {
        circularJson: (circ) => {
        let cache = [];
        JSON.stringify(circ, (key, value) => {
            if (typeof value === 'object' && value !== null) {
            // Duplicate reference found, discard key
            if (cache.includes(value)) return;
        
            // Store value in our collection
            cache.push(value);
            }
            return value;
        });
    }
}